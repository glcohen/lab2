package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		return getValue() + " C";
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius(getValue());
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) (((float)9/5) * (getValue()) + 32));
	}
	
	@Override
	public Temperature toKelvin() {
		return new Kelvin(getValue() + 273);
	}
	
}


package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature {
	
	public Fahrenheit(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		return getValue() + " F";
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius((float) (((float)5/9) * (getValue() - 32)));
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(getValue());
	}
	
	public Temperature toKelvin() {
		Temperature tmp = new Celsius(getValue());
		return new Kelvin(tmp.getValue() + 273);
	}
	
}


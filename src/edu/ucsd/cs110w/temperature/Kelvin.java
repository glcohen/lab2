/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbk
 *
 */
public class Kelvin extends Temperature {

	public Kelvin(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		return getValue() + " K";
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius((float)(getValue() - 273.15));
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float)((getValue() - 273.15) * 1.8 + 32));
	}
	
	public Temperature toKelvin() {
		return this;
	}

}
